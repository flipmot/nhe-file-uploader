﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static NHEFileUploader.Models.Enums;

namespace NHEFileUploader.Dialogs
{
    /// <summary>
    /// Interaction logic for YesNoDialog.xaml
    /// </summary>
    public partial class YesNoDialog : Window
    {
        public YesNoResult Result = YesNoResult.Cancel;

        public YesNoDialog(bool inShowCancel = false)
        {
            InitializeComponent();
            if (inShowCancel) { CancelButton.Visibility = Visibility.Visible; }
            else { CancelButton.Visibility = Visibility.Collapsed; }
        }

        public YesNoDialog(string inDisplayText, bool inShowCancel = false)
        {
            InitializeComponent();
            MainText.Text = inDisplayText;
            if (inShowCancel) { CancelButton.Visibility = Visibility.Visible; }
            else { CancelButton.Visibility = Visibility.Collapsed; }
        }

        #region Getters & Setters
        public string displyText
        {
            set { MainText.Text = value; }
        }
        public bool showConcel
        {
            set
            {
                if (value) { CancelButton.Visibility = Visibility.Visible;}
                else { CancelButton.Visibility = Visibility.Collapsed; }
            }
        }

        #endregion

        private void YesButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Result = YesNoResult.Yes;
            DialogResult = true;
        }

        private void NoButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Result = YesNoResult.No;
            DialogResult = true;
        }

        private void CancelButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Result = YesNoResult.Cancel;
            DialogResult = true;
        }
    }
}
