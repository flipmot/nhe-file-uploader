﻿using NHEFileUploader.Models;
using NHEFileUploader.Repository;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace NHEFileUploader.Dialogs
{
    /// <summary>
    /// Interaction logic for ProjectCode.xaml
    /// </summary>
    public partial class ProjectCode : Window
    {
        public string result = "";
        public List<Project.ProjectPlot> plots = null;

        public ProjectCode()
        {
            InitializeComponent();

            this.ProjCode.Focus();

        }

        private void OkButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            plots = new Portal().GetAllPlotNames(ProjCode.Text ?? "");
            if (ProjCode.Text != "" && plots != null)
            {
                result = ProjCode.Text;
                this.Close();
            }
            else
            {
                ErrText.Visibility = Visibility.Visible;
            }
        }

        private void CancelButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            result = "";
            this.Close();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //this.DragMove();
        }
    }
}
