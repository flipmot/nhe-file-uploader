﻿using NHEFileUploader.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using static NHEFileUploader.Models.Project;

namespace NHEFileUploader.Dialogs
{
    /// <summary>
    /// Interaction logic for ProjectPlot.xaml
    /// </summary>
    public partial class ProjectSelectPlot : Window
    {
        public string Result = "";
        public ProjectSelectPlot(List<ProjectPlot> inPlotList)
        {
            InitializeComponent();
            plotList = inPlotList;
        }


        public List<ProjectPlot> plotList
        {
            set
            {
                ProjPlot.ItemsSource = value.Where(r => !Session.usedPlots
                                                            .Any(t => t.plotId == r.plotId))
                                                            .ToList();

                ProjPlot.DisplayMemberPath = "plotName";
                ProjPlot.SelectedValuePath = "plotId";

            }
        }
        private void OkButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Session.usedPlots.Add((ProjectPlot)ProjPlot.SelectedItem);
            Result = ((ProjectPlot)ProjPlot.SelectedItem).plotId;
            DialogResult = true;
        }

        private void CancelButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

            DialogResult = true;
        }
    }
}
