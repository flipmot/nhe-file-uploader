﻿using RaptorEngine;
using System;
using System.Diagnostics;
using System.Reflection;
using static RaptorEngine.RegistryTools;

namespace NHEFileUploader.Repository
{
    class General
    {
        public static class ProcessMessage
        {
            public static string UploadComplete = "Upload complete. Would you like to upload more files to this project?";
            public static string UploadMismatch = "Project number does not match ";
            public static string ProjectComplete = "Project update complete.";
            public static string RegenComplete = "Material regeneration complete";
            public static string PlotCancelled = "Plot selection cancelled.";
            public static string FinalisingUpload = "Finalising file upload";
            public static string StartPolling = "Regenerating Materials List...";


        }

        public static class ErrorMessage
        {
            public static string UploadFailed = "Failed to upload file: ";
            public static string UploadError = "Error uploading file";
            public static string ProjectFailed = "Project update failed. Contact support.";
            public static string SaveCopyFailed = "Failed to save copy of file.";
            public static string ValidateError = "Error Validating Project Code";
            public static string PollingError = "Error Polling Portal";
            public static string CompleteError = "Error Completeing Upload";
            public static string URLError = "Error Finalising upload - No URL retured.";
        }

        public static class FolderLocation
        {
            public static string SaveCopyTo = @"c:\Temp\";
        }

        public void VersionCheck()
        {
            try
            {
                RegistryKeyIdentifier myRegKey = new RegistryTools().GetProgramRegKeyIdentifier("ProjectFileUploader");

                var myVersion = new RegistryTools().GetRegistryKeyValue(myRegKey);


                Assembly assembly = Assembly.GetExecutingAssembly();
                FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
                string version = fileVersionInfo.ProductVersion;
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "VersionCheck");
            }



        }
    }
}
