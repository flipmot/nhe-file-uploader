﻿using Microsoft.Win32.SafeHandles;
using NHEFileUploader.Components;
using NHEFileUploader.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using static NHEFileUploader.Models.ExcelInfo;
using static NHEFileUploader.Models.Project;
using static NHEFileUploader.Repository.General;
using Excel = Microsoft.Office.Interop.Excel;

namespace NHEFileUploader.Repository
{
    internal class ExcelTools
    {
        bool disposed = false;
        // Instantiate a SafeHandle instance.
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        private static Excel.Application mvXlapp = null;
        private List<ProjectPlot> mvPlots = null;
        public List<WorkbookItem> GetOpenWorkbooks()
        {
            List<WorkbookItem> outList = new List<WorkbookItem>();
            try
            {
                mvXlapp = (Excel.Application)Marshal.GetActiveObject("Excel.Application");

                Console.WriteLine(mvXlapp.Workbooks.Count.ToString());

                for (int i = 1; i < mvXlapp.Workbooks.Count + 1; i++)
                {
                    if (ValidateProjectCode(GetProjectCode(mvXlapp.Workbooks[i])))
                    {

                        WorkbookItem myWB = new WorkbookItem(i, mvXlapp.Workbooks[i].Name, mvXlapp.Workbooks[i].FullName, GetProjectCode(mvXlapp.Workbooks[i]), mvPlots, Enums.FileSource.Process);
                        outList.Add(myWB);
                    }
                }
            }
            catch
            { }
            return outList;
        }

        public bool CopyEstimate(string inFileName)
        {
            bool outBool = false;
            try
            {
                mvXlapp = (Excel.Application)Marshal.GetActiveObject("Excel.Application");

                Console.WriteLine(mvXlapp.Workbooks.Count.ToString());

                for (int i = 1; i < mvXlapp.Workbooks.Count + 1; i++)
                {
                    if (mvXlapp.Workbooks[i].Name == inFileName)
                    {
                        mvXlapp.DisplayAlerts = false;
                        mvXlapp.Workbooks[i].SaveCopyAs(FolderLocation.SaveCopyTo + inFileName); //ToDo: Enum file loc
                        outBool = true;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "CopyEstimate");
            }
            return outBool;
        }

        public void DeleteTempFile(string inFileName)
        {
            File.Delete(FolderLocation.SaveCopyTo + inFileName); //ToDo: Enum file loc
        }

        public string GetProjectCode(Excel.Workbook inWorkbook)
        {
            string outString = "";

            try
            {
                outString = inWorkbook.Sheets[SheetName.JobDetails].Range[FieldRange.ProjectCode].value ?? "";

            }
            catch
            {
                outString = "";
            }
            finally
            {
                Marshal.FinalReleaseComObject(inWorkbook);
            }
            return outString;
        }

        public bool ValidateProjectCode(string inProjectCode)
        {
            bool outBool = false;
            try
            {
                if (inProjectCode != "")
                {
                    mvPlots = new Portal().GetAllPlotNames(inProjectCode ?? "").Where(r => !Session.usedPlots
                                                            .Any(t => t.plotId == r.plotId))
                                                            .ToList();
                    
                    if (mvPlots != null)
                    {
                        outBool = true;
                    }
                }
            }
            catch
            {
                outBool = false;
            }
            return outBool;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
                // Free any other managed objects here.
                if (mvXlapp != null)
                {
                    Marshal.FinalReleaseComObject(mvXlapp);
                    mvXlapp = null;
                }

            }

            disposed = true;
        }
        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
