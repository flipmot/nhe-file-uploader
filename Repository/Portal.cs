﻿using NHEFileUploader.Models;
using PortalGatewayV2.Models;
using PortalGatewayV2.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Script.Serialization;
using static NHEFileUploader.Models.PortalInfo;
using static NHEFileUploader.Models.Project;
using static NHEFileUploader.Repository.General;

namespace NHEFileUploader.Repository
{
    public class Portal
    {
        //PortalGatewayV2.Service.PortalRequests mvPortalReq;
        private GenericResp mvFullResponse = new GenericResp();

        public GenericResp FullResponse
        {
            get { return mvFullResponse; }
            set { mvFullResponse = value; }
        }

        public string UploadProjectFile(string inProjCode, string inPlot, Dictionary<dynamic, string> inFiles)
        {
            string outString = "";
            try
            {
                DateTime myProcTimer = DateTime.Now;
                SimpleLog.Info("Process: UploadProjectFile - Started");

                Request myReqData = new Request("uploadNheFile", Request.GatewayType.Estimator, inAuthToken: GetApplicationToken());

                Dictionary<string, string> myInputs = new Dictionary<string, string>();
                myInputs.Add("refNumber", inProjCode);
                myInputs.Add("plot", inPlot);

                myReqData.inputs = myInputs;

                myReqData.files = inFiles;

                DateTime Started = DateTime.Now;

                Response.PortalResponse myPortalResp = new PortalRequests.PostRequest("Estimator", myReqData).Send();

                if (myPortalResp.success && myPortalResp.data != null)
                {
                    PortalInfo.UploadNHEFileResp myResponse = new JavaScriptSerializer().Deserialize<PortalInfo.UploadNHEFileResp>(myPortalResp.data);
                    FullResponse = new JavaScriptSerializer().Deserialize<PortalInfo.GenericResp>(myPortalResp.data);

                    if (myResponse.error)
                    {

                        throw new Exception(myResponse.message);
                    }
                    else
                    {

                        if (myResponse.data != null)
                        {

                            foreach (var item in myResponse.data)
                            {
                                if (item.Key.ToString() == "buttonToDisplay")
                                {
                                    outString = item.Value.ToString();
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        FullResponse = new JavaScriptSerializer().Deserialize<GenericResp>(myPortalResp.errMessage);
                    }
                    catch
                    {
                        throw new Exception(myPortalResp.errMessage);
                    }
                }

                var myProcTime = (DateTime.Now - myProcTimer).TotalSeconds;
                SimpleLog.Info("Process: UploadProjectFile - Finished  Process Time: " + myProcTime.ToString() + " seconds");
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "UploadProjectFile", popUpMsg: false, inMsg: ErrorMessage.UploadError);
                outString = null;
            }

            return outString;
        }

        //public async Task UploadProjectFile(string inProjCode, string inPlot, Dictionary<dynamic, string> inFiles, Action<bool, string> inCompAction, IProgress<int> inProgress)
        //{
        //    string outString = "";
        //    try
        //    {
        //        DateTime myProcTimer = DateTime.Now;
        //        SimpleLog.Info("Process: UploadProjectFile - Started");

        //        Request myReqData = new Request("uploadNheFile", Request.GatewayType.Estimator, inAuthToken: GetApplicationToken());

        //        Dictionary<string, string> myInputs = new Dictionary<string, string>();
        //        myInputs.Add("refNumber", inProjCode);
        //        myInputs.Add("plot", inPlot);

        //        myReqData.inputs = myInputs;

        //        myReqData.files = inFiles;

        //        DateTime Started = DateTime.Now;

        //        new PortalRequests.PostRequest("Estimator", myReqData).Send((result) => UploadFileComplete(inCompAction,result),inProgress);
        //    }
        //    catch (Exception e)
        //    {
        //        ErrorHandler.Log(e, "UploadProjectFile", popUpMsg: false, inMsg: ErrorMessage.UploadError);
        //        outString = null;
        //    }
        //}

        private void UploadFileComplete(Action<string> inCompAction, Response.PortalResponse inResponse)
        {
            string outString = "";
            try
            {
                if (inResponse.success && inResponse.data != null)
                {
                    PortalInfo.UploadNHEFileResp myResponse = new JavaScriptSerializer().Deserialize<PortalInfo.UploadNHEFileResp>(inResponse.data);


                    if (myResponse.error)
                    {
                        throw new Exception(myResponse.message);
                    }
                    else
                    {

                        if (myResponse.data != null)
                        {
                            foreach (var item in myResponse.data)
                            {
                                if (item.Key.ToString() == "buttonToDisplay")
                                {
                                    outString = item.Value.ToString();
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                { throw new Exception(inResponse.errMessage); }
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "UploadProjectFile", popUpMsg: false, inMsg: ErrorMessage.UploadError);
                outString = null;
            }

            inCompAction(outString);
        }

        //public bool UploadProjectFile(string inProjCode, string inPlot, List<dynamic> inFiles)
        //{
        //    bool outBool = false;
        //    try
        //    {
        //        SimpleLog.Info("Process: UploadProjectFile - Started");

        //        PortalInfo.PortalRequest myJSONCls = new PortalInfo.PortalRequest()
        //        {
        //            action = "uploadNheFile", //ToDo: Enum this
        //            token = GetApplicationToken(),
        //            refNumber = inProjCode
        //            //plot = inPlot
        //        };

        //        PortalGateway gateway = new PortalGateway();

        //        //Send post request to PHP

        //        PortalGateway.PostResponse PostResponse = gateway.SendNA(URLs.EstimatorExternalLocal, myJSONCls, inFiles);

        //        SimpleLog.Info("Process: UploadProjectFile - Response: Success = " + PostResponse.Success + " - Message = " + PostResponse.Message ?? "");

        //        if (PostResponse.Success)
        //        {
        //            outBool = true;
        //            //Removed below as only looking for an upload request success of true now 30/03/20
        //            //################################################################################
        //            //PortalInfo.UploadNHEFileResp myResponse = new JavaScriptSerializer().Deserialize<PortalInfo.UploadNHEFileResp>(PostResponse.Response);
        //            //if (myResponse.error)
        //            //{
        //            //    throw new Exception(myResponse.message);
        //            //}
        //            //else
        //            //{
        //            //    Console.WriteLine(myResponse.data.ToString());
        //            //    foreach (var item in myResponse.data)
        //            //    {
        //            //        if (item.Key == "pollingRequired")
        //            //        {
        //            //            outBool = item.Value;
        //            //        }
        //            //        Console.WriteLine(item.Key + " " + item.Value);
        //            //    }
        //            //}
        //        }
        //        else
        //        {
        //            throw new Exception(PostResponse.Message);
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        new ErrorHandler(e, "UploadProjectFile", popUpMsg: false, inMsg: "Error Uploading Project File");
        //        throw;
        //    }
        //    return outBool;
        //}
        public List<ProjectPlot> GetAllPlotNames(string inProjCode)
        {
            List<ProjectPlot> outPlots = null;
            try
            {
                DateTime myProcTimer = DateTime.Now;
                SimpleLog.Info("Process: GetAllPlotNames - Started");

                Request myReqData = new Request("getAllPlots", Request.GatewayType.Estimator, inAuthToken: GetApplicationToken());

                Dictionary<string, string> myInputs = new Dictionary<string, string>();
                myInputs.Add("refNumber", inProjCode);

                myReqData.inputs = myInputs;

                Response.PortalResponse myPortalResp = new PortalRequests.PostRequest("Estimator", myReqData).Send();

                if (myPortalResp.success && myPortalResp.data != null)
                {
                    PortalInfo.GetAllPlotNamesResp myResponse = new JavaScriptSerializer().Deserialize<PortalInfo.GetAllPlotNamesResp>(myPortalResp.data);
                    FullResponse = new JavaScriptSerializer().Deserialize<PortalInfo.GenericResp>(myPortalResp.data);

                    if (myResponse.error)
                    {
                        throw new Exception(myResponse.message);
                    }
                    else
                    {
                        outPlots = new List<ProjectPlot>();
                        if (myResponse.data != null && myResponse.data.allPlots != null)
                        {
                            int i = 0;
                            foreach (var item in myResponse.data.allPlots)
                            {
                                ProjectPlot myPlot = new ProjectPlot(i, item.id, item.name);

                                outPlots.Add(myPlot);
                                i++;
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        FullResponse = new JavaScriptSerializer().Deserialize<GenericResp>(myPortalResp.errMessage);
                    }
                    catch
                    {
                        throw new Exception(myPortalResp.errMessage);
                    }
                }

                var myProcTime = (DateTime.Now - myProcTimer).TotalSeconds;
                SimpleLog.Info("Process: GetAllPlotNames - Finished  Process Time: " + myProcTime.ToString() + " seconds");
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "GetAllPlotNames", popUpMsg: false, inMsg: ErrorMessage.ValidateError);
                outPlots = null;
            }


            return outPlots;
        }

        public List<string> GetProjectCurrentSteps(string inProjCode)
        {
            List<string> outList = null;
            try
            {
                DateTime myProcTimer = DateTime.Now;
                SimpleLog.Info("Process: GetProjectCurrentSteps - Started");

                Request myReqData = new Request("getProjectCurrentSteps", Request.GatewayType.Estimator, inAuthToken: GetApplicationToken());

                Dictionary<string, string> myInputs = new Dictionary<string, string>();
                myInputs.Add("refNumber", inProjCode);

                myReqData.inputs = myInputs;

                Response.PortalResponse myPortalResp = new PortalRequests.PostRequest("Estimator", myReqData).Send();

                if (myPortalResp.success && myPortalResp.data != null)
                {
                    PortalInfo.GetProjCurrentStepsResp myResponse = new JavaScriptSerializer().Deserialize<PortalInfo.GetProjCurrentStepsResp>(myPortalResp.data);

                    if (myResponse.error)
                    {
                        throw new Exception(myResponse.message);
                    }
                    else
                    {
                        outList = new List<string>();
                        if (myResponse.data != null)
                        {
                            foreach (var item in myResponse.data.currentSteps)
                            {
                                outList.Add(item.ToString());
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        FullResponse = new JavaScriptSerializer().Deserialize<GenericResp>(myPortalResp.errMessage);
                    }
                    catch
                    {
                        throw new Exception(myPortalResp.errMessage);
                    }
                }

                var myProcTime = (DateTime.Now - myProcTimer).TotalSeconds;
                SimpleLog.Info("Process: GetProjectCurentSteps - Finished  Process Time: " + myProcTime.ToString() + " seconds");
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "GetProjectCurentSteps", popUpMsg: false, inMsg: ErrorMessage.PollingError);
                outList = null;
            }


            return outList;
        }

        public string AllFilesUploaded()
        {
            string outString = "";
            try
            {
                DateTime myProcTimer = DateTime.Now;
                SimpleLog.Info("Process: AllFilesUploaded - Started");

                Request myReqData = new Request("completeFileUploadStep", Request.GatewayType.Estimator, inAuthToken: GetApplicationToken());

                Dictionary<string, string> myInputs = new Dictionary<string, string>();
                myInputs.Add("refNumber", Session.currentProj);

                myReqData.inputs = myInputs;

                Response.PortalResponse myPortalResp = new PortalRequests.PostRequest("Estimator", myReqData).Send();

                if (myPortalResp.success && myPortalResp.data != null)
                {
                    PortalInfo.UploadNHEFileResp myResponse = new JavaScriptSerializer().Deserialize<PortalInfo.UploadNHEFileResp>(myPortalResp.data);
                    FullResponse = new JavaScriptSerializer().Deserialize<PortalInfo.GenericResp>(myPortalResp.data);

                    if (myResponse.error)
                    {
                        throw new Exception(myResponse.message);
                    }
                    else
                    {

                        foreach (var item in myResponse.data)
                        {
                            if (item.Key == "flightDeckLink")
                            {
                                outString = item.Value;
                            }
                            Console.WriteLine(item.Key + " " + item.Value);
                        }
                    }
                }
                else
                {
                    try
                    {
                        FullResponse = new JavaScriptSerializer().Deserialize<GenericResp>(myPortalResp.errMessage);
                    }
                    catch
                    {
                        throw new Exception(myPortalResp.errMessage);
                    }
                }

                var myProcTime = (DateTime.Now - myProcTimer).TotalSeconds;
                SimpleLog.Info("Process: UploadProjectFile - Finished  Process Time: " + myProcTime.ToString() + " seconds");
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "AllFilesUploaded", popUpMsg: false, inMsg: ErrorMessage.CompleteError);
                outString = "Failed To Complete upload";
            }

            return outString;
        }

        public bool ReGenMaterials()
        {
            bool outBool = false;
            try
            {
                DateTime myProcTimer = DateTime.Now;
                SimpleLog.Info("Process: AllFilesUploaded - Started");

                Request myReqData = new Request("reRunMaterialList", Request.GatewayType.Estimator, inAuthToken: GetApplicationToken());

                Dictionary<string, string> myInputs = new Dictionary<string, string>();
                myInputs.Add("refNumber", Session.currentProj);

                myReqData.inputs = myInputs;

                Response.PortalResponse myPortalResp = new PortalRequests.PostRequest("Estimator", myReqData).Send();

                if (myPortalResp.success && myPortalResp.data != null)
                {
                    PortalInfo.UploadNHEFileResp myResponse = new JavaScriptSerializer().Deserialize<PortalInfo.UploadNHEFileResp>(myPortalResp.data);

                    if (myResponse.error)
                    {
                        throw new Exception(myResponse.message);
                    }
                    else
                    {

                        foreach (var item in myResponse.data)
                        {
                            if (item.Key == "pollingRequired")
                            {
                                outBool = !item.Value;
                                break;
                            }
                            else if (item.Key == "flightDeckLink")
                            {
                                outBool = false;
                                break;
                            }
                            Console.WriteLine(item.Key + " " + item.Value);
                        }
                    }
                }
                else
                {
                    try
                    {
                        FullResponse = new JavaScriptSerializer().Deserialize<GenericResp>(myPortalResp.errMessage);
                    }
                    catch
                    {
                        throw new Exception(myPortalResp.errMessage);
                    }
                }

                var myProcTime = (DateTime.Now - myProcTimer).TotalSeconds;
                SimpleLog.Info("Process: UploadProjectFile - Finished  Process Time: " + myProcTime.ToString() + " seconds");
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "AllFilesUploaded", popUpMsg: false, inMsg: ErrorMessage.CompleteError);
                outBool = false;
            }

            return outBool;
        }

        public void ShowPortalPage(string inURL)
        {
            Process myProcess = new Process();

            try
            {
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = inURL;
                myProcess.Start();
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "ShowPortalPage");
            }
        }

        private string GetApplicationToken()
        {
            string outString = "";
            var attrs = Assembly.GetExecutingAssembly().GetCustomAttributes(false).OfType<System.Runtime.InteropServices.GuidAttribute>();
            if (attrs.Any())
            {
                outString = (attrs.First().Value);
            }
            return outString;
        }


        public byte[] XlFileToByte(string inFile)
        {
            byte[] outBytes = null;
            using (MemoryStream ms = new MemoryStream())
            using (FileStream file = new FileStream(inFile, FileMode.Open, FileAccess.Read))
            {
                byte[] bytes = new byte[file.Length];
                file.Read(bytes, 0, (int)file.Length);
                ms.Write(bytes, 0, (int)file.Length);

                outBytes = ms.ToArray();
            }

            return outBytes;
        }

    }
}
