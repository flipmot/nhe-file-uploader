﻿using System;
using System.Windows;

namespace NHEFileUploader.Repository
{
    internal static class ErrorHandler
    {
        //static Debug.LogReader mvLogRead = new Debug.LogReader();
        public static void Log(Exception err, string method, bool sendToSlack = true, bool popUpMsg = true, bool msgBox = false, string inMsg = "", bool logError = true)
        {
            Session.lastException = err;

            if (sendToSlack)
            {
                //ToDo: Slack is currently disabled
                //new Slack().SendErrorReport("An error has occured with " + method, err.ToString(), "Aviator Menu", includePopup: false);
            }

            if (popUpMsg && inMsg != "")
            {
                new Raptor2.PopUp.Pop(inMsg);
            }

            if (msgBox && inMsg != "")
            {
                MessageBox.Show("An error has occured - " + inMsg);
            }

            if (logError)
            {
                SimpleLog.Log(err);
                if (Session.isDebug)
                {
                    //Application.Current.Dispatcher.Invoke((Action)delegate
                    //{
                    //    mvLogRead.ShowLog();
                    //});
                }

            }
        }
    }
}
