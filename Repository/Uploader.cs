﻿using NHEFileUploader.Components;
using NHEFileUploader.Dialogs;
using NHEFileUploader.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using static NHEFileUploader.Repository.General;
using ErrMsg = NHEFileUploader.Repository.General.ErrorMessage;
using ProcMsg = NHEFileUploader.Repository.General.ProcessMessage;

namespace NHEFileUploader.Repository
{
    class Uploader
    {
        public Portal mvPortal;

        public Uploader(Portal inPortal)
        {
            mvPortal = inPortal;
        }
        public Uploader() { }
        public void UploadFile(WorkbookItem inWB, Action<bool, string> uploadComp)
        {
            Task task = Task.Run(() =>
            {
                bool myPass = true;
                string myMsg = "";
                try
                {
                    string myPlot = "";
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        if (Session.currentPlots != null && Session.currentPlots.Count > 1 && Session.currentPlots.Count > Session.usedPlots.Count)
                        {
                            ProjectSelectPlot projectPlot = new ProjectSelectPlot(Session.currentPlots);
                            projectPlot.ShowDialog();
                            myPlot = projectPlot.Result;
                            if (myPlot == "")
                            {
                                myPass = false;
                                myMsg = ProcMsg.PlotCancelled;
                            }
                        }
                    });

                    Dictionary<dynamic, string> myFiles = new Dictionary<dynamic, string>();

                    if (inWB.IsSelected && myPass)
                    {

                        switch (inWB.FileSource)
                        {
                            case Enums.FileSource.File:
                                Portal myPortal1 = new Portal();
                                myFiles.Add(myPortal1.XlFileToByte(inWB.FilePath), inWB.FileName);
                                inWB.DisplyButton = myPortal1.UploadProjectFile(inWB.ProjCode, myPlot, myFiles);
                                inWB.NextStep = inWB.DisplyButton;
                                if (myPortal1.FullResponse.error)
                                {
                                    myPass = false;
                                    myMsg = myPortal1.FullResponse.message;
                                }


                                Console.WriteLine(Session.lastException);
                                break;
                            case Enums.FileSource.Process:
                                //Save file
                                ExcelTools myXlTools = new ExcelTools();
                                try
                                {
                                    if (myXlTools.CopyEstimate(inWB.FileName))
                                    {
                                        Portal myPortal2 = new Portal();
                                        Thread.Sleep(2000); //To ensure excel has finished saving.
                                        myFiles.Add(myPortal2.XlFileToByte(FolderLocation.SaveCopyTo + inWB.FileName), inWB.FileName);
                                        inWB.DisplyButton = myPortal2.UploadProjectFile(inWB.ProjCode, myPlot, myFiles);
                                        inWB.NextStep = inWB.DisplyButton;
                                        if (myPortal2.FullResponse.error)
                                        {
                                            myPass = false;
                                            myMsg = myPortal2.FullResponse.message;
                                        }
                                        break;
                                    }
                                    else { throw new Exception(ErrMsg.SaveCopyFailed); }
                                }
                                catch (Exception err)
                                {
                                    myPass = false;
                                    myMsg = err.Message;
                                }
                                finally
                                {
                                    myXlTools.DeleteTempFile(inWB.FileName);
                                    myXlTools.Dispose();
                                }

                                break;
                        }

                    }


                    uploadComp(myPass, myMsg);
                }
                catch (Exception e)
                {
                    ErrorHandler.Log(e, "UploadFile", popUpMsg: false, inMsg: ErrMsg.UploadError);
                    uploadComp(false, e.Message);

                }
            });
        }

        //public void UploadFile(WorkbookItem inWB, Action<bool, string> uploadComp, IProgress<int> inProgress)
        //{
        //    Task task = Task.Run(() =>
        //    {
        //        bool myPass = true;
        //        string myMsg = "";
        //        try
        //        {
        //            string myPlot = "";
        //            Application.Current.Dispatcher.Invoke((Action)delegate
        //            {
        //                if (Session.currentPlots != null && Session.currentPlots.Count > 1 && Session.currentPlots.Count > Session.usedPlots.Count)
        //                {
        //                    ProjectSelectPlot projectPlot = new ProjectSelectPlot(Session.currentPlots);
        //                    projectPlot.ShowDialog();
        //                    myPlot = projectPlot.Result;
        //                    if (myPlot == "")
        //                    {
        //                        myPass = false;
        //                        myMsg = ProcMsg.PlotCancelled;
        //                    }
        //                }
        //            });

        //            Dictionary<dynamic, string> myFiles = new Dictionary<dynamic, string>();

        //            if (inWB.IsSelected && myPass)
        //            {

        //                switch (inWB.FileSource)
        //                {
        //                    case Enums.FileSource.File:
        //                        myFiles.Add(new Portal().XlFileToByte(inWB.FilePath), "XLSM");
        //                        inWB.DisplyButton = new Portal().UploadProjectFile(inWB.ProjCode, myPlot, myFiles, uploadComp, inProgress);
        //                        inWB.NextStep = inWB.DisplyButton;
        //                        break;
        //                    case Enums.FileSource.Process:
        //                        //Save file
        //                        ExcelTools myXlTools = new ExcelTools();
        //                        try
        //                        {
        //                            if (myXlTools.CopyEstimate(inWB.FileName))
        //                            {
        //                                Thread.Sleep(2000); //To ensure excel has finished saving.
        //                                myFiles.Add(new Portal().XlFileToByte(FolderLocation.SaveCopyTo + inWB.FileName), "XLSM");
        //                                inWB.DisplyButton = new Portal().UploadProjectFile(inWB.ProjCode, myPlot, myFiles, inProgress);
        //                                inWB.NextStep = inWB.DisplyButton;
        //                                break;
        //                            }
        //                            else { throw new Exception(ErrMsg.SaveCopyFailed); }
        //                        }
        //                        catch (Exception err)
        //                        {
        //                            myPass = false;
        //                            myMsg = err.Message;
        //                        }
        //                        finally
        //                        {
        //                            myXlTools.DeleteTempFile(inWB.FileName);
        //                            myXlTools.Dispose();
        //                        }

        //                        break;
        //                }
        //            }

        //            uploadComp(myPass, myMsg);
        //        }
        //        catch (Exception e)
        //        {
        //            ErrorHandler.Log(e, "UploadFile", popUpMsg: false, inMsg: ErrMsg.UploadError);
        //            uploadComp(false, e.Message);

        //        }
        //    });
        //}


        //public void UploadFiles(List<WorkbookItem> inWBs, Action<bool> uploadComp)
        //{
        //    Task task = Task.Run(() =>
        //    {
        //        try
        //        {
        //            List<dynamic> myFiles = new List<dynamic>();
        //            foreach (dynamic item in inWBs)
        //            {
        //                if (item.isSelected)
        //                {

        //                    switch (item.fileSource)
        //                    {
        //                        case Enums.FileSource.File:
        //                            myFiles.Add(new Portal().XlFileToByte(item.filePath));
        //                            bool myResult = new Portal().UploadProjectFile(item.projCode, myFiles);
        //                            //Check to see if polling is required
        //                            if (myResult)
        //                            {
        //                                //Thread.Sleep(5000);
        //                                MessageBox.Show("Polling Reqd");
        //                            }
        //                            else
        //                            {
        //                                //Thread.Sleep(5000);
        //                                MessageBox.Show("No Polling Reqd");
        //                            }
        //                            break;
        //                        case Enums.FileSource.Process:

        //                            break;
        //                    }
        //                }
        //            }
        //            uploadComp(true);
        //        }
        //        catch
        //        {
        //            uploadComp(false);
        //            throw;
        //        }
        //    });
        //}
        public void PollMaterialRegen(string inProjCode, Action<bool> pollingComp)
        {

            try
            {
                bool myPolling = true;

                int myCnt = 1;
                while (myPolling)
                {
                    myCnt++;
                    if (PollPortal(inProjCode))
                    {
                        myPolling = false;
                    }

                    Thread.Sleep(1000);

                }
                pollingComp(true);
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "PollForComplete", false, false);
                pollingComp(false);
            }
        }
        public void PollForComplete(string inProjCode, Action<bool> pollingComp)
        {

            try
            {
                bool myPolling = true;

                int myCnt = 1;
                while (myPolling)
                {
                    myCnt++;
                    if (PollPortal(inProjCode))
                    {
                        myPolling = false;
                    }

                    Thread.Sleep(1000);

                }
                pollingComp(true);
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "PollForComplete", false, false);
                pollingComp(false);
            }
        }

        private bool PollPortal(string inProjCode)
        {
            bool outBool = true;
            if (mvPortal == null)
            {
                mvPortal = new Portal();
            }

            foreach (var item in mvPortal.GetProjectCurrentSteps(inProjCode))
            {
                if (item.Contains("reGenerateXML"))
                {
                    outBool = false;
                }
            }

            return outBool;
        }
    }
}
