﻿using System;
using System.Collections.Generic;
using System.Windows;
using static NHEFileUploader.Models.Enums;
using static NHEFileUploader.Models.Project;

namespace NHEFileUploader.Repository
{
    internal static class Session
    {


        #region Member Variables
        private static MainWindow mvMainWindow;

        private static string mvCurrentProj = "";
        private static List<ProjectPlot> mvCurrentPlots = new List<ProjectPlot>();
        private static List<ProjectPlot> mvUsedPlots = new List<ProjectPlot>();

        private static CurrentState mvCurrentState = CurrentState.None;

        private static bool mvLoggingEnabled = true;
        private static string mvLogLocation = ".\\Log"; //Todo; Enum or static var this with other folder locations

        private static Exception mvLastException;
        #endregion

        #region Class Getters/Setters
        public static MainWindow mainWindow
        {
            get { return mvMainWindow; }
            set { mvMainWindow = value; }
        }
        public static string currentProj
        {
            get { return mvCurrentProj; }
            set { mvCurrentProj = value; }
        }

        public static List<ProjectPlot> currentPlots
        {
            get { return mvCurrentPlots; }
            set { mvCurrentPlots = value; }
        }

        public static List<ProjectPlot> usedPlots
        {
            get { return mvUsedPlots; }
            set { mvUsedPlots = value; }
        }

        public static CurrentState currentState
        {
            get { return mvCurrentState; }
            set
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    mvMainWindow.StateChanged(value);
                });
                mvCurrentState = value;
            }
        }

        public static bool loggingEnabled
        {
            get { return mvLoggingEnabled; }
            set
            {
                if (value)
                {
                    SimpleLog.SetLogFile(logDir: ".\\Log", prefix: "Log_", writeText: false);
                }

                if (Session.isDebug) { SimpleLog.LogLevel = SimpleLog.Severity.Info; }
                else { SimpleLog.LogLevel = SimpleLog.Severity.Warning; }

                mvLoggingEnabled = value;
            }
        }

        public static bool isDebug
        {
            get
            {
                bool isDebug = false;

#if (DEBUG)
                isDebug = true;
#else
                isDebug = false;
#endif

                return isDebug;
            }
        }

        public static Exception lastException
        {
            get { return mvLastException; }
            set { mvLastException = value; }
        }
        #endregion

    }
}
