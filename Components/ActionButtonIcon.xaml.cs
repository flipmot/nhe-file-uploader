﻿using System.Windows.Controls;
using System.Windows.Media;
using static NHEFileUploader.Models.Colors;
using static NHEFileUploader.Models.Enums;

namespace NHEFileUploader.Components
{
    /// <summary>
    /// Interaction logic for ActionButton.xaml
    /// </summary>
    public partial class ActionButtonIcon : UserControl
    {
        #region Member Variables
        private bool Selected { get; set; }
        private Brush mvBackColour1;// = ColourConst.CorpBlue;
        private Brush mvBackColour2 = ColourConst.CorpBlueLighter;
        private Brush mvBackColour3 = ColourConst.CorpYellow;
        private Brush mvFontColour1 = ColourConst.CorpYellow;
        private Brush mvFontColour2 = ColourConst.CorpWhite;
        private Brush mvFontColour3 = ColourConst.CorpBlue;
        private Brush mvFontColour4 = ColourConst.CorpGrey;
        private string mvToolTipTitle = "ToolTip";
        private string mvToolTipLine1 = "TootTip Text";
        private string mvToolTipLine2 = "TootTip Text";
        #endregion

        #region Getters & Setters
        new public bool Enabled
        {
            get { return this.IsEnabled; }
            set
            {
                this.IsEnabled = value;
                if (value)
                {
                    MainIcon.Foreground = mvFontColour1;
                }
                else
                {
                    MainIcon.Foreground = mvFontColour4;
                }
            }

        }

        public ActionButtonIcon()
        {
            Selected = false;
            InitializeComponent();
            ResetButton();
        }

        public Brush BackColour1
        {
            get { return mvBackColour1; }
            set
            {
                mvBackColour1 = value;
                MainBorder.Background = value;
            }
        }

        public Brush BackColour2
        {
            get { return mvBackColour2; }
            set { mvBackColour2 = value; }
        }

        public Brush BackColour3
        {
            get { return mvBackColour3; }
            set { mvBackColour3 = value; }
        }

        public Brush FontColour1
        {
            get { return mvFontColour1; }
            set
            {
                mvFontColour1 = value;
                MainIcon.Foreground = value;
            }
        }

        public Brush FontColour2
        {
            get { return mvFontColour2; }
            set { mvFontColour2 = value; }
        }

        public Brush FontColour3
        {
            get { return mvFontColour3; }
            set { mvFontColour3 = value; }
        }

        public Brush FontColour4
        {
            get { return mvFontColour4; }
            set { mvFontColour4 = value; }
        }

        public ActionButtonType ButtonType
        {
            set
            {
                switch (value)
                {
                    case ActionButtonType.Upload:
                        MainIcon.Icon = FontAwesome.WPF.FontAwesomeIcon.CloudUpload;
                        MainIcon.ToolTip = "Click to upload file to portal.";
                        MainIcon.Spin = false;
                        break;
                    case ActionButtonType.Open:
                        MainIcon.Icon = FontAwesome.WPF.FontAwesomeIcon.FolderOpen;
                        MainIcon.ToolTip = "Click to load a file from disk.";
                        MainIcon.Spin = false;
                        break;
                    case ActionButtonType.Finish:
                        MainIcon.Icon = FontAwesome.WPF.FontAwesomeIcon.FlagCheckered;
                        MainIcon.ToolTip = "Click to finish uploading files.";
                        MainIcon.Spin = false;
                        break;
                    case ActionButtonType.Refresh:
                        MainIcon.Icon = FontAwesome.WPF.FontAwesomeIcon.Refresh;
                        MainIcon.ToolTip = "Click to refresh list of open files.";
                        MainIcon.Spin = false;
                        break;
                    case ActionButtonType.Loading:
                        MainIcon.Icon = FontAwesome.WPF.FontAwesomeIcon.Spinner;
                        MainIcon.ToolTip = "Please wait";
                        MainIcon.Spin = true;
                        break;
                    case ActionButtonType.NextStep:
                        MainIcon.Icon = FontAwesome.WPF.FontAwesomeIcon.StepForward;
                        MainIcon.ToolTip = "Click to push project to next step.";
                        MainIcon.Spin = false;
                        break;
                    case ActionButtonType.Regenerate:
                        MainIcon.Icon = FontAwesome.WPF.FontAwesomeIcon.Cogs;
                        MainIcon.ToolTip = "Click to regenerate materials list.";
                        MainIcon.Spin = false;
                        break;

                }
            }
        }

        public string ToolTipTitle
        {
            get { return mvToolTipTitle; }
            set
            {
                mvToolTipTitle = value;
            }
        }

        public string ToolTipLine1
        {
            get { return mvToolTipLine1; }
            set { mvToolTipLine1 = value; }
        }
        public string ToolTipLine2
        {
            get { return mvToolTipLine2; }
            set { mvToolTipLine2 = value; }
        }

        public double ButtonHeight
        {
            set
            {
                MainBorder.Height = value;
                MainBorder.Width = value;
            }
        }
        #endregion

        #region Component Methods
        private void ResetButton()
        {
            MainIcon.Foreground = FontColour1;
            //MainBorder.Background = ColourConst.DiscRed;
            MainBorder.Background = BackColour1;
        }

        public void Select()
        {
            Selected = true;

            //MainText.Foreground = new SolidColorBrush(Color.FromRgb(250, 222, 32));
        }

        public void Deselect()
        {
            Selected = false;

        }

        #endregion

        #region Event Methods
        private void Border_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            MainIcon.Foreground = mvFontColour2;
            MainBorder.Background = mvBackColour2;
        }

        private void Border_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (!Selected)
            {
                MainIcon.Foreground = mvFontColour1;
                MainBorder.Background = mvBackColour1;
            }
        }

        private void Border_MouseDown(object sender, System.Windows.Input.MouseEventArgs e)
        {
            MainIcon.Foreground = mvFontColour3;
            MainBorder.Background = mvBackColour3;
        }

        #endregion
    }
}
