﻿using NHEFileUploader.Repository;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;


namespace NHEFileUploader.Components.Logging
{
    /// <summary>
    /// Interaction logic for LogEntry.xaml
    /// </summary>
    public partial class LogEntry : UserControl
    {
        private Models.LogReader.LogEntriesLogEntryException mvLogException;
        public LogEntry()
        {
            InitializeComponent();
        }

        public LogEntry(Models.LogReader.LogEntriesLogEntry inLogItem)
        {
            InitializeComponent();
            logType = (SimpleLog.Severity)Enum.Parse(typeof(SimpleLog.Severity), inLogItem.Severity);
            logDate = inLogItem.Date;
            logSource = inLogItem.Source;
            if (inLogItem.Exception != null)
            {
                logMessage = inLogItem.Exception.Message;
                logStack = inLogItem.Exception.StackTrace;
                logException = inLogItem.Exception;
            }
            else if (inLogItem.Message != null)
            {
                logMessage = inLogItem.Message;
            }
        }
        #region Getters & Setters
        public SimpleLog.Severity logType
        {
            set
            {
                Type.Text = value.ToString();
                switch (value)
                {
                    case SimpleLog.Severity.Info:
                        MainBorder.BorderBrush = Brushes.Green;
                        TypeIcon.Icon = FontAwesome.WPF.FontAwesomeIcon.Info;
                        TypeIcon.Foreground = Brushes.Black;
                        break;
                    case SimpleLog.Severity.Warning:
                        MainBorder.BorderBrush = Brushes.Orange;
                        TypeIcon.Icon = FontAwesome.WPF.FontAwesomeIcon.ExclamationCircle;
                        TypeIcon.Foreground = Brushes.Black;
                        break;
                    case SimpleLog.Severity.Error:
                    case SimpleLog.Severity.Exception:
                        MainBorder.BorderBrush = Brushes.Red;
                        TypeIcon.Icon = FontAwesome.WPF.FontAwesomeIcon.ExclamationTriangle;
                        TypeIcon.Foreground = Brushes.Red;
                        break;
                }
            }
        }

        public string logDate
        {
            set { Date.Text = value; }
        }

        public string logMessage
        {
            set { Message.Text = value; }
        }

        public string logSource
        {
            set { Source.Text = value; }
        }

        public string logStack
        {
            set { Stack.Text = value; }
        }
        public Models.LogReader.LogEntriesLogEntryException logException
        {
            get { return mvLogException; }
            set { mvLogException = value; }
        }
        #endregion
        private void ViewStack_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (mvLogException != null)
            {
                if (ViewStack.Icon == FontAwesome.WPF.FontAwesomeIcon.Search)
                {
                    StackLbl.Visibility = Visibility.Visible;
                    Stack.Visibility = Visibility.Visible;
                    ViewStack.Icon = FontAwesome.WPF.FontAwesomeIcon.Close;
                }
                else
                {
                    StackLbl.Visibility = Visibility.Collapsed;
                    Stack.Visibility = Visibility.Collapsed;
                    ViewStack.Icon = FontAwesome.WPF.FontAwesomeIcon.Search;
                }
                //MessageBox.Show("Exception Source: " + mvLogException.Source + "\n" + "Stack Trace: " + mvLogException.StackTrace);
            }
        }
    }
}
