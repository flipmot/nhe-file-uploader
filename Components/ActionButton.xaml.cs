﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using static NHEFileUploader.Models.Colors;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NHEFileUploader.Components
{
    /// <summary>
    /// Interaction logic for ActionButton.xaml
    /// </summary>
    public partial class ActionButton : UserControl
    {
        private bool Selected { get; set; }
        private Brush mvBackColour1;// = ColourConst.CorpBlue;
        private Brush mvBackColour2 = ColourConst.CorpBlueLighter;
        private Brush mvBackColour3 = ColourConst.CorpYellow;
        private Brush mvFontColour1 = ColourConst.CorpYellow;
        private Brush mvFontColour2 = ColourConst.CorpWhite;
        private Brush mvFontColour3 = ColourConst.CorpBlue;
        private string mvToolTipTitle = "ToolTip";
        private string mvToolTipLine1 = "TootTip Text";
        private string mvToolTipLine2 = "TootTip Text";

        public ActionButton()
        {
            Selected = false;
            InitializeComponent();
            ResetButton();
        }



        public Brush BackColour1
        {
            get { return mvBackColour1; }
            set
            {
                mvBackColour1 = value;
                MainBorder.Background = value;
            }
        }

        public Brush BackColour2
        {
            get { return mvBackColour2; }
            set { mvBackColour2 = value; }
        }

        public Brush BackColour3
        {
            get { return mvBackColour3; }
            set { mvBackColour3 = value; }
        }

        public Brush FontColour1
        {
            get { return mvFontColour1; }
            set
            {
                mvFontColour1 = value;
                MainText.Foreground = value;
            }
        }

        public Brush FontColour2
        {
            get { return mvFontColour2; }
            set { mvFontColour2 = value; }
        }

        public Brush FontColour3
        {
            get { return mvFontColour3; }
            set { mvFontColour3 = value; }
        }
        public string Text
        {
            get { return MainText.Text; }
            set { MainText.Text = value; }
        }

        public string ToolTipTitle
        {
            get { return mvToolTipTitle; }
            set
            {
                mvToolTipTitle = value;
            }
        }

        public string ToolTipLine1
        {
            get { return mvToolTipLine1; }
            set { mvToolTipLine1 = value; }
        }
        public string ToolTipLine2
        {
            get { return mvToolTipLine2; }
            set { mvToolTipLine2 = value; }
        }


        public double ButtonFontSize
        {
            get { return MainText.FontSize; }
            set { MainText.FontSize = value; }
        }

        public double ButtonHeight
        {
            get { return MainBorder.Height; }
            set { MainBorder.Height = value; }
        }
        private void ResetButton()
        {
            MainText.Foreground = FontColour1;
            //MainBorder.Background = ColourConst.DiscRed;
            MainBorder.Background = BackColour1;
        }
        private void Border_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            MainText.Foreground = mvFontColour2;
            MainBorder.Background = mvBackColour2;
        }

        private void Border_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (!Selected)
            {
                MainText.Foreground = mvFontColour1;
                MainBorder.Background = mvBackColour1;
            }
        }

        private void Border_MouseDown(object sender, System.Windows.Input.MouseEventArgs e)
        {
            MainText.Foreground = mvFontColour3;
            MainBorder.Background = mvBackColour3;
        }

        public void Select()
        {
            Selected = true;

            //MainText.Foreground = new SolidColorBrush(Color.FromRgb(250, 222, 32));
        }

        public void Deselect()
        {
            Selected = false;

        }


    }
}
