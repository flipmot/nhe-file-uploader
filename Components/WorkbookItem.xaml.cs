﻿using NHEFileUploader.Models;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using static NHEFileUploader.Models.Colors;
using static NHEFileUploader.Models.Project;

namespace NHEFileUploader.Components
{
    /// <summary>
    /// Interaction logic for WorkbookItem.xaml
    /// </summary>
    public partial class WorkbookItem : UserControl
    {
        public event Action<WorkbookItem> ItemSelected;
        private bool mvIsSelected;
        private bool mvIsUploading;
        private Enums.FileSource mvFileSource;
        private int mvFileId;
        private string mvFilePath = "";
        private string mvFileName = "";
        private string mvProjCode = "";
        private List<ProjectPlot> mvPlots;
        private bool mvUploaded;
        private string mvNextStep = "";
        private string mvDisplayButton = "";
        public WorkbookItem()
        {
            InitializeComponent();
        }

        public WorkbookItem(int inFileId, string inFileName, string inFilePath, string inProjCode, List<ProjectPlot> inPlots, Enums.FileSource inFileSource)
        {
            InitializeComponent();
            mvFileId = inFileId;
            FileName = inFileName;
            FilePath = inFilePath;
            FileSource = inFileSource;
            ProjCode = inProjCode;
            Plots = inPlots;
        }

        #region Getters & Setters

        public string DisplyButton
        {
            get { return mvDisplayButton; }
            set { mvDisplayButton = value; }
        }
        public string NextStep
        {
            get { return mvNextStep; }
            set { mvNextStep = value; }
        }

        public bool Uploaded
        {
            get { return mvUploaded; }
            set
            {
                mvUploaded = value;
                this.Dispatcher.Invoke((Action)delegate
                {
                    if (value)
                    {
                        FileIcon.Visibility = Visibility.Collapsed;
                        XLIcon.Visibility = Visibility.Collapsed;
                        UploadIcon.Visibility = Visibility.Visible;
                        ProgBar.Value = 100;
                        FileInfoText.Text = "Uploaded To Project: " + ProjCode;
                    }

                });
            }
        }
        public Enums.FileSource FileSource
        {
            get { return mvFileSource; }
            set
            {
                mvFileSource = value;
                switch (value)
                {
                    case Enums.FileSource.File:
                        FileIcon.Visibility = Visibility.Visible;
                        break;
                    case Enums.FileSource.Process:
                        XLIcon.Visibility = Visibility.Visible;
                        break;
                }
            }
        }

        public int FileId
        {
            get { return mvFileId; }
            set { mvFileId = value; }
        }

        public string FileName
        {
            get { return mvFileName; }
            set
            {
                FileNameText.Text = value;
                mvFileName = value;
            }
        }

        public string FilePath
        {
            get { return mvFilePath; }
            set { mvFilePath = value; }
        }

        public string FileInfo
        {
            get { return FileInfoText.Text; }
            set { FileInfoText.Text = value; }
        }

        public string ProjCode
        {
            get { return mvProjCode; }
            set
            {
                FileInfoText.Text = "Project: " + value;
                mvProjCode = value;
            }
        }

        public List<ProjectPlot> Plots
        {
            get { return mvPlots; }
            set { mvPlots = value; }
        }

        public bool IsUploading
        {
            get { return mvIsUploading; }
            set
            {
                mvIsUploading = value;
                if (value)
                {
                    FileIcon.Visibility = Visibility.Collapsed;
                    XLIcon.Visibility = Visibility.Collapsed;
                    SpinIcon.Visibility = Visibility.Visible;
                    FileInfoText.Text = "Uploading file, please wait....";

                    System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                    dispatcherTimer.Tick += loadingTimer_Tick;
                    dispatcherTimer.Interval = new TimeSpan(0, 0, 6);
                    dispatcherTimer.Start();
                    ProgBar.Value = 0;
                    ProgBar.Visibility = Visibility.Visible;
                }
                else if (mvUploaded)
                {
                    SpinIcon.Visibility = Visibility.Collapsed;
                    UploadIcon.Visibility = Visibility.Visible;
                    FileInfoText.Text = "Uploaded To Project: " + ProjCode;
                    ProgBar.Value = 0;
                    ProgBar.Visibility = Visibility.Collapsed;
                }
                else
                {
                    ProgBar.Value = 0;
                    ProgBar.Visibility = Visibility.Collapsed;
                    SpinIcon.Visibility = Visibility.Collapsed;
                    switch (mvFileSource)
                    {
                        case Enums.FileSource.File:
                            FileIcon.Visibility = Visibility.Visible;
                            FileInfoText.Text = "Project: " + ProjCode;
                            break;
                        case Enums.FileSource.Process:
                            XLIcon.Visibility = Visibility.Visible;
                            FileInfoText.Text = "Project: " + ProjCode;
                            break;
                    }
                }
            }
        }
        public bool IsSelected
        {
            get { return mvIsSelected; }
            set
            {
                if (!mvIsUploading)
                {
                    if (value)
                    {
                        Border.Background = ColourConst.FadedGreen;
                        //FileInfo.Text = "Click the Upload File button to upload";
                        ItemSelected(this);
                    }
                    else
                    {
                        Border.Background = ColourConst.CorpWhite;
                        //FileInfo.Text = "Click to select this file";
                    }
                    mvIsSelected = value;
                }
            }
        }
        #endregion

        private void loadingTimer_Tick(object sender, EventArgs e)
        {
            // code goes here
            ProgBar.Value++;
        }

        private void Border_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            IsSelected = !mvIsSelected;
        }
    }
}
