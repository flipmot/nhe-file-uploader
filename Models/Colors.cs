﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace NHEFileUploader.Models
{
    public static class Colors
    { 

        public static class ColourConst
        {
            public static Brush CorpBlue = "#11458c".ToBrush();
            public static Brush HighBlue = "#11458c".ToBrush();
            public static Brush AccTeal = "#009781".ToBrush();
            public static Brush DiscRed = "#c20012".ToBrush();
            public static Brush CorpYellow = "#ffd600".ToBrush();
            public static Brush CorpGrey = "#dadada".ToBrush();
            public static Brush CorpWhite = "#ffffff".ToBrush();
            public static Brush FadedGreen = "#1A7346".ToBrush();
            public static Brush AccGreen = "#217346".ToBrush();
            public static Brush CorpBlueLighter = "#0d55b6".ToBrush();
            public static Brush FadedText = "#00438f".ToBrush();
            public static Brush ReqdPink = "#f5a6b4".ToBrush();
        }
        public static SolidColorBrush ToBrush(this string HexColorString)
        {
            return (SolidColorBrush)(new BrushConverter().ConvertFrom(HexColorString));
        }
    }
}
