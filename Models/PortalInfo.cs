﻿namespace NHEFileUploader.Models
{
    public class PortalInfo
    {
        public class PortalRequest
        {
            public string token { get; set; }
            public string action { get; set; }
            public string refNumber { get; set; }
            public string plot { get; set; }
            public dynamic data { get; set; }
        }

        public class PortalResponse
        {
            public bool error { get; set; }
            public string message { get; set; }
            public string code { get; set; }
            public string errorDetail { get; set; }

            public object requestId { get; set; }
            public object time { get; set; }
        }
        public class UploadNHEFileResp : PortalResponse
        {
            public dynamic data { get; set; }
        }

        public class GetAllPlotNamesResp : PortalResponse
        {
            public Data data { get; set; }
            public class Data
            {
                public Allplotname[] allPlots { get; set; }

                public class Allplotname
                {
                    public string id { get; set; }
                    public string name { get; set; }
                }
            }

        }
        public class GetProjCurrentStepsResp : PortalResponse
        {
            public Data data { get; set; }
            public class Data
            {
                //public object[] currentSteps { get; set; }
                public dynamic currentSteps { get; set; }
            }
        }

        public class AllFilesUploadedResp : PortalResponse
        {
            public dynamic data { get; set; }
        }

        public class GenericResp : PortalResponse
        {
            public dynamic data { get; set; }
        }
    }
}
