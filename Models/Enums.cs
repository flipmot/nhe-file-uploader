﻿namespace NHEFileUploader.Models
{
    public class Enums
    {
        public enum FileSource
        {
            File,
            Process
        }

        public enum YesNoResult
        {
            Yes,
            No,
            Cancel
        }
        public enum CurrentState
        {
            None,
            Validating,
            Uploading,
            Polling
        }

        public enum ActionButtonType
        {
            Upload,
            Open,
            Finish,
            Refresh,
            Loading,
            NextStep,
            Regenerate,
        }

        public enum NextAction
        {
            reGenerateMaterialsList,
            completeStep
        }
    }
}
