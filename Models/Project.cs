﻿namespace NHEFileUploader.Models
{
    public class Project
    {

        public class ProjectPlot
        {
            public ProjectPlot(int inIndex, string inId, string inName)
            {
                plotIndex = inIndex;
                plotId = inId;
                plotName = inName;
            }
            public int plotIndex { get; set; }
            public string plotId { get; set; }
            public string plotName { get; set; }
        }


    }
}
