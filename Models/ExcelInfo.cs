﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHEFileUploader.Models
{
    class ExcelInfo
    {
        public class FieldRange
        {
            public static string ProjectCode = "NHEJobRef";
        }

        public class SheetName
        {
            public static string JobDetails = "START WINDOW";
        }
    }
}
