﻿using NHEFileUploader.Repository;
using System;
using System.IO;
using System.Windows;
using System.Xml.Linq;
using System.Xml.Serialization;
using static NHEFileUploader.Models.LogReader;


namespace NHEFileUploader.Debug
{
    /// <summary>
    /// Interaction logic for LogReader.xaml
    /// </summary>
    public partial class LogReader : Window
    {
        public LogReader()
        {
            InitializeComponent();
            GetLog();

            System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 1, 0);
            dispatcherTimer.Start();
        }

        public void ShowLog()
        {
            GetLog();
            if (this.IsLoaded)
            {
                this.Show();
            }
            
        }

        public LogEntries GetLog()
        {
            LogEntries outList;
            //DateTime myLogDate = new DateTime(2020, 03, 30);
            //XDocument myLogXml = SimpleLog.GetLogFileAsXml(myLogDate);
            XDocument myLogXml = SimpleLog.GetLogFileAsXml();

            XmlSerializer mySer = new XmlSerializer(typeof(LogEntries));

            using (StringReader mySR = new StringReader(myLogXml.ToString()))
            {
                outList = (LogEntries)mySer.Deserialize(mySR);
            }

            LogStack.Children.Clear();

            foreach (var item in outList.LogEntry)
            {
                Components.Logging.LogEntry newEntry = new Components.Logging.LogEntry(item);

                LogStack.Children.Insert(0, newEntry);

            }


            return outList;
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            GetLog();
        }
    }
}
