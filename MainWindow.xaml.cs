﻿using Microsoft.Win32;
using NHEFileUploader.Components;
using NHEFileUploader.Dialogs;
using NHEFileUploader.Models;
using NHEFileUploader.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using static NHEFileUploader.Models.Enums;
using static NHEFileUploader.Models.Project;
using ErrMsg = NHEFileUploader.Repository.General.ErrorMessage;
using Excel = Microsoft.Office.Interop.Excel;
using ProcMsg = NHEFileUploader.Repository.General.ProcessMessage;

namespace NHEFileUploader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private static Excel.Application mvXlapp = null;

        public MainWindow()
        {
            InitializeComponent();
            ResetButtons();
            Session.mainWindow = this;
            Session.loggingEnabled = true;
            //var i = 0;
            //Console.WriteLine(1 / i);
            GetOpenWorkbooks();
        }

        public void FileSelected(WorkbookItem inWB)
        {
            foreach (dynamic item in FileStack.Children)
            {
                if (item.FileId != inWB.FileId)
                {
                    item.IsSelected = false;
                }
            }

            if (inWB.Uploaded)
            {
                //ToDo: Show relevent buttons depending on next step
                switch (inWB.DisplyButton.ToLower())
                {
                    case "regeneratemateriallist":
                        NextStep.Enabled = true;
                        Finish.Enabled = false;
                        break;
                    case "complete":
                        NextStep.Enabled = false;
                        Finish.Enabled = true;
                        break;
                    default:
                        ResetButtons();
                        break;
                }
            }
        }

        public void StateChanged(CurrentState inState)
        {
            switch (inState)
            {
                case CurrentState.None:
                    ResetButtons();
                    StateBorder.Visibility = Visibility.Collapsed;
                    break;
                case CurrentState.Validating:
                    break;
                case CurrentState.Uploading:
                    StateBorder.Visibility = Visibility.Visible;
                    StateInfoText.Text = "Uploading files for project" + Session.currentProj;
                    break;
                case CurrentState.Polling:
                    StateBorder.Visibility = Visibility.Visible;
                    StateInfoText.Text = "Updating project details" + Session.currentProj;
                    break;
            }
        }

        #region Private Methods
        private void UploadFile()
        {
            try
            {

                new Thread(() =>
                {
                    this.Dispatcher.Invoke(delegate ()
                    {
                        this.SendFile.ButtonType = ActionButtonType.Loading;
                        this.SendFile.IsEnabled = false;
                        this.LoadFile.IsEnabled = false;
                        this.FileStack.IsEnabled = false;
                    });
                }).Start();



                //List<WorkbookItem> myWBs = new List<WorkbookItem>();
                foreach (dynamic item in FileStack.Children)
                {
                    if (item.IsSelected)
                    {
                        if (Session.currentProj != "" && Session.currentProj != item.ProjCode)
                        {
                            UploadComplete(false, ProcMsg.UploadMismatch + Session.currentProj, (WorkbookItem)item);
                            break;
                        }
                        else if (Session.currentProj == "")
                        {
                            Session.currentProj = item.ProjCode;
                            Session.currentPlots = item.Plots;
                        }

                        Session.currentState = CurrentState.Uploading;
                        item.IsUploading = true;
                        new Uploader().UploadFile((WorkbookItem)item, (result, failMsg) => UploadComplete(result, failMsg, (WorkbookItem)item));
                        break;
                        //myWBs.Add(item);
                    }
                }
                //new Uploader().UploadFiles(myWBs,(result) => UploadComplete(result));
            }
            catch (Exception err) { ErrorHandler.Log(err, "SendFile_MouseDown"); }
        }

        private void LoadFromDisk()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "NHE Files (*.xlsm)|*.XLSM";
            if (openFileDialog.ShowDialog() == true)
            {
                foreach (dynamic item in FileStack.Children)
                {
                    if (item.FileId == 999)
                    {
                        FileStack.Children.Remove(item);
                        break;
                    }
                }
                string[] myNameSplit = openFileDialog.FileName.Split('\\');
                string myResult = "";
                List<ProjectPlot> myPlots = null;

                if (Session.currentProj == "")
                {
                    Dialogs.ProjectCode GetProjCode = new Dialogs.ProjectCode();
                    GetProjCode.Dispatcher.Invoke(delegate
                    {
                        GetProjCode.ShowDialog();
                        myResult = GetProjCode.result;
                        if (myResult != "")
                        {
                            myPlots = GetProjCode.plots.Where(r => !Session.usedPlots
                                                                .Any(t => t.plotId == r.plotId))
                                                                .ToList();
                        }
                    });
                }
                else
                {
                    myResult = Session.currentProj;
                    myPlots = Session.currentPlots.Where(r => !Session.usedPlots
                                                                .Any(t => t.plotId == r.plotId))
                                                                .ToList();
                }


                if (myResult != "")
                {
                    WorkbookItem myWB = new WorkbookItem(999, myNameSplit[myNameSplit.Length - 1], openFileDialog.FileName, myResult, myPlots, Enums.FileSource.File);
                    FileStack.Children.Insert(0, myWB);
                    myWB.ItemSelected += FileSelected;
                    myWB.IsSelected = true;
                }
            }
        }

        private void GetOpenWorkbooks()
        {
            ExcelTools myXlTools = null;
            try
            {
                FileStack.Children.Clear();
                myXlTools = new ExcelTools();
                List<WorkbookItem> myWBs = myXlTools.GetOpenWorkbooks();
                foreach (var item in myWBs)
                {
                    FileStack.Children.Add(item);
                    item.ItemSelected += FileSelected;
                    item.IsSelected = false;
                }

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                myXlTools.Dispose();
                myXlTools = null;
            }
        }

        private async Task ShowWait(bool inWait, string inWaitText)
        {
            this.Dispatcher.Invoke(delegate ()
            {
                this.SendFile.IsEnabled = false;
                this.LoadFile.IsEnabled = false;
                this.FileStack.IsEnabled = false;
                this.WaitText.Text = inWaitText;
                if (inWait) { this.PleaseWait.Visibility = Visibility.Visible; }
                else { this.PleaseWait.Visibility = Visibility.Collapsed; }

            });
        }

        private void ResetButtons()
        {
            LoadFile.ButtonType = ActionButtonType.Open;
            Refresh.ButtonType = ActionButtonType.Refresh;
            Finish.ButtonType = ActionButtonType.Finish;
            Finish.Enabled = false;
            NextStep.ButtonType = ActionButtonType.Regenerate;
            NextStep.Enabled = false;
            SendFile.ButtonType = ActionButtonType.Upload;

        }

        private async void CompleteUpload()
        {
            try
            {
                Session.currentState = CurrentState.Polling;

                Task showWait = ShowWait(true, General.ProcessMessage.FinalisingUpload);

                await showWait;
                string myURL = new Portal().AllFilesUploaded();
                if (myURL == "")
                {
                    throw new Exception(General.ErrorMessage.URLError);
                }
                new Portal().ShowPortalPage(myURL);
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "CompleteUpload");
            }
        }

        private async void StartPolling()
        {
            try
            {
                Session.currentState = CurrentState.Polling;

                Task showWait = ShowWait(true, General.ProcessMessage.StartPolling);

                await showWait;
                Portal myPortal = new Portal();
                bool poll = myPortal.ReGenMaterials();
                if (!poll)
                {
                    ReGenComplete(true);
                }
                else
                {
                    new Uploader(myPortal).PollForComplete(Session.currentProj, (result) => ReGenComplete(result));
                }
                if (myPortal.FullResponse.data != null)
                {
                    bool myLinkFound = false;
                    foreach (var item in myPortal.FullResponse.data)
                    {
                        if (item.Key == "flightDeckLink")
                        {
                            myLinkFound = true;
                            myPortal.ShowPortalPage(item.Value);

                            break;
                        }
                    }
                    if (!myLinkFound)
                    {
                        throw new Exception(General.ErrorMessage.UploadError);
                    }
                }
                else
                {
                    throw new Exception(General.ErrorMessage.UploadError);
                }
            }
            catch (Exception e)
            {
                ErrorHandler.Log(e, "StartPolling");
            }
        }
        #endregion

        #region Callback Tasks
        public async Task UploadComplete(bool inSuccess, string inFailReason, WorkbookItem inWB)
        {
            new Thread(() =>
            {
                this.Dispatcher.Invoke(delegate ()
                {
                    this.FileStack.IsEnabled = true;
                    this.SendFile.ButtonType = ActionButtonType.Upload;
                    this.SendFile.IsEnabled = true;
                    this.LoadFile.IsEnabled = true;
                    foreach (dynamic item in this.FileStack.Children)
                    {
                        item.IsUploading = false;
                        if (item.IsSelected)
                        {
                            FileSelected(item);
                        }
                    }
                });
            }).Start();

            if (inSuccess)
            {
                inWB.Uploaded = true;
                YesNoResult myResult = YesNoResult.Cancel;
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    YesNoDialog YesNo = new YesNoDialog();
                    YesNo.displyText = General.ProcessMessage.UploadComplete;
                    YesNo.ShowDialog();

                    myResult = YesNo.Result;
                });

                if (myResult == YesNoResult.No)
                {
                    Task showWait = ShowWait(true, "Upload Complete");

                    await showWait;

                    //ToDo: need to do the correct task for the action.
                    switch (inWB.NextStep.ToLower())
                    {
                        case "regeneratemateriallist":
                            CompleteUpload();
                            break;
                        case "completestep":
                            StartPolling();
                            break;
                        default:
                            break;
                    }
                    //new Uploader().PollForComplete(inWB.ProjCode, (result) => PollingComplete(result));
                }
                else
                {
                    this.Dispatcher.Invoke(delegate ()
                    {
                        switch (inWB.DisplyButton.ToLower())
                        {
                            case "regeneratemateriallist":
                                this.NextStep.Enabled = true;
                                break;
                            case "completestep":
                                this.Finish.Enabled = true;
                                break;
                            default:
                                break;
                        }
                        //this.Finish.Visibility = Visibility.Visible;
                    });

                }
            }
            else { MessageBox.Show(ErrMsg.UploadFailed + inFailReason); }

        }

        public void PollingComplete(bool inSuccess)
        {
            Session.currentProj = "";
            //Session.currentPlot = "";
            Session.currentState = CurrentState.None;
            if (inSuccess)
            {
                this.Dispatcher.Invoke(delegate ()
                {
                    this.Finish.Visibility = Visibility.Collapsed;
                    this.WaitText.Text = ProcMsg.ProjectComplete;
                    this.WaitOk.Visibility = Visibility.Visible;
                    this.WaitProg.Visibility = Visibility.Collapsed;
                });
            }
            else
            {
                this.Dispatcher.Invoke(delegate ()
                {
                    this.WaitText.Text = ErrMsg.ProjectFailed;
                    this.WaitOk.Visibility = Visibility.Visible;
                    this.WaitProg.Visibility = Visibility.Collapsed;
                });
            }


        }

        public void ReGenComplete(bool inSuccess)
        {
            Session.currentProj = "";
            //Session.currentPlot = "";
            Session.currentState = CurrentState.None;
            if (inSuccess)
            {
                this.Dispatcher.Invoke(delegate ()
                {
                    this.Finish.Visibility = Visibility.Collapsed;
                    this.WaitText.Text = ProcMsg.ProjectComplete;
                    this.WaitOk.Visibility = Visibility.Visible;
                    this.WaitProg.Visibility = Visibility.Collapsed;
                });
            }
            else
            {
                this.Dispatcher.Invoke(delegate ()
                {
                    this.WaitText.Text = ErrMsg.ProjectFailed;
                    this.WaitOk.Visibility = Visibility.Visible;
                    this.WaitProg.Visibility = Visibility.Collapsed;
                });
            }


        }
        #endregion

        #region Event Methods
        private void SendFile_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            UploadFile();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                this.DragMove();
            }

        }

        private void LoadFile_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            LoadFromDisk();
        }

        private void WaitOk_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Dispatcher.Invoke(delegate ()
            {
                this.SendFile.IsEnabled = true;
                this.LoadFile.IsEnabled = true;
                this.FileStack.IsEnabled = true;
                this.PleaseWait.Visibility = Visibility.Collapsed;
                this.WaitOk.Visibility = Visibility.Collapsed;
                this.WaitProg.Visibility = Visibility.Visible;
            });
        }

        private async void NextStep_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            StartPolling();
        }

        private void Finish_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            CompleteUpload();
        }
        private void Refresh_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            GetOpenWorkbooks();
            Session.currentState = CurrentState.None;
        }

        private void LogoImage_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Debug.LogReader myLogRead = new Debug.LogReader();

            myLogRead.Show();

        }

        private void CloseIcon_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Close();
        }

        #endregion
    }
}
